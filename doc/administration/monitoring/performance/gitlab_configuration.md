---
stage: Service Management
group: Respond
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GitLab Configuration

DETAILS:
**Tier:** Free, Premium, Ultimate
**Offering:** Self-managed

See [Prometheus integration](../prometheus/index.md).

## Pending migrations

When any migrations are pending, the metrics are disabled until the migrations
have been performed.

Read more on:

- [Introduction to GitLab Performance Monitoring](index.md)
- [Grafana Install/Configuration](grafana_configuration.md)
